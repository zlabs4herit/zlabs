package net.herit.ini.svc.r2d2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import herit.ini.adaptor.Call;
import herit.ini.adaptor.openapi.AppAdaptor;
import herit.ini.adaptor.openapi.call.ReceivedRequestCall;
import herit.ini.adaptor.openapi.certification.ServiceCertification;
import herit.ini.adaptor.openapi.http.HTTPRequest;
import herit.ini.scf.SCF;
import herit.ini.scf.SCFContext;
import net.herit.ini.svc.r2d2.module.grepgrep.GrepGrepHttpClient;
import net.herit.ini.svc.r2d2.module.shell.R2D2Shell;

public class R2D2Service extends SCF {
	
	
	public static String LOGGER_NAME;
	public static String CALL_LOGGER_NAME;
	private SCFContext context;
	
	public Logger callLog;
	private String className = this.getClass().getSimpleName();
	private AppAdaptor appAdaptor;
	
	
	@Override
	public void activate() throws Exception {
		
		context = getSCFContext();

		CALL_LOGGER_NAME = context.getServiceID().getCallLoggerName();

		callLog = LoggerFactory.getLogger(CALL_LOGGER_NAME);
		
		R2D2Properties.getInst().setProperty(context);
		
		appAdaptor = (AppAdaptor) getSCFContext().getProtocolAdaptor("APP_ADAPTOR");
		ServiceCertification serviceUriCertification = appAdaptor.createServiceCertification(getSCFContext().getServiceID());
		
		
		String grepgrepUri = R2D2Properties.getInst().getGrepgrepUri();
		String shellUri = "/shell/";
		
		
		serviceUriCertification.setUriPrefix(grepgrepUri, shellUri);
		
		appAdaptor.registCertification(serviceUriCertification);
	}

	@Override
	public boolean execute(Call call) {
		
		String classAndMethodName = className + ".execute";
		ReceivedRequestCall recvCall = (ReceivedRequestCall)call;
		HTTPRequest request = recvCall.getHttpRequest();
			
		callLog.debug("classAndMethodName={}", classAndMethodName);
		
		GrepGrepHttpClient httpClient = new GrepGrepHttpClient();
		try {
			if(recvCall.getPath().equals("/shell/")) {
				R2D2Shell shell = new R2D2Shell();
				
				shell.execute(request, recvCall);
			}
			else {
				httpClient.init(request, recvCall);
			}
		} catch (InterruptedException e) {
			callLog.error(e.toString());
		} catch (Exception e) {
			callLog.error(e.toString());
		}
		return true;
	}

	@Override
	public void gc() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inactivate() {
		
		appAdaptor.unregistCertification(context.getServiceID());
	}
	
}
