package net.herit.ini.svc.r2d2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import herit.ini.scf.SCFContext;

public class R2D2Properties {
	
	private SCFContext context;
	private Logger callLog;
	
	private String grepgrepUri;
	private String grepgrepHost;
	
	
	public static final R2D2Properties inst = new R2D2Properties();
	
	public static final R2D2Properties getInst() {
		return inst;
	}

	public void setProperty(SCFContext context) {
		//
		try {
			
			callLog = LoggerFactory.getLogger(context.getServiceID().getCallLoggerName());
			this.context = context;

			grepgrepUri = context.getProfile().getProperty("r2d2_grepgrep_uri");
			grepgrepHost = context.getProfile().getProperty("r2d2_grepgrep_host");
			
		}
		catch(Exception e) {
			
		}
	}

	public String getGrepgrepUri() {
		return grepgrepUri;
	}

	public String getGrepgrepHost() {
		return grepgrepHost;
	}
	
}
