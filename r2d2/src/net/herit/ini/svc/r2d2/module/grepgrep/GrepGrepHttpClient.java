package net.herit.ini.svc.r2d2.module.grepgrep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import herit.ini.adaptor.openapi.call.ReceivedRequestCall;
import herit.ini.adaptor.openapi.http.HTTPRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import net.herit.ini.svc.r2d2.R2D2Properties;
import net.herit.ini.svc.r2d2.R2D2Service;

public class GrepGrepHttpClient {

	private Logger callLog = LoggerFactory.getLogger(R2D2Service.CALL_LOGGER_NAME);
	private String className = this.getClass().getSimpleName();
	private String[] host = R2D2Properties.getInst().getGrepgrepHost().split(":");
	private String ip = host[0];
	private int port = Integer.parseInt(host[1]);
	
	public void init(final HTTPRequest httpReq, final ReceivedRequestCall recvCall) throws InterruptedException {
		//
		EventLoopGroup eventLoop = new NioEventLoopGroup();
		
		try {
			
			Bootstrap b = new Bootstrap();
			b.group(eventLoop);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel channel) throws Exception {
					// TODO Auto-generated method stub
					String classAndMethodName = className+".initChannel";
					callLog.debug("classAndMethodName={}", classAndMethodName);

					channel.pipeline().addLast(new HttpClientCodec());
					channel.pipeline().addLast(new HttpObjectAggregator(1048576));
					channel.pipeline().addLast(new GrepGrepHttpClientHandler(httpReq, recvCall));
				}
			});
            
            Channel ch = b.connect(ip, port).sync().channel();
            
            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, httpReq.getUri());
            
            request.headers().set(HttpHeaders.Names.HOST, R2D2Properties.getInst().getGrepgrepHost());
            request.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
            
			callLog.debug("request={}", request.toString());

            ch.writeAndFlush(request);
            ch.closeFuture().sync();
            
		}
		finally {
			eventLoop.shutdownGracefully();

		}
	}
}
