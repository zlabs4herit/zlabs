package net.herit.ini.svc.r2d2.module.grepgrep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import herit.ini.adaptor.openapi.call.ReceivedRequestCall;
import herit.ini.adaptor.openapi.http.HTTPRequest;
import herit.ini.adaptor.openapi.http.HTTPResponse;
import herit.ini.adaptor.openapi.http.property.HTTPResponseStatus;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponse;
import net.herit.ini.svc.r2d2.R2D2Service;

public class GrepGrepHttpClientHandler extends SimpleChannelInboundHandler<HttpObject> {

	private Logger callLog = LoggerFactory.getLogger(R2D2Service.CALL_LOGGER_NAME);
	private String className = this.getClass().getSimpleName();
	private HTTPRequest httpReq;
	private ReceivedRequestCall recvCall;
	
	public GrepGrepHttpClientHandler(HTTPRequest httpReq, ReceivedRequestCall recvCall) {
		this.httpReq = httpReq;
		this.recvCall = recvCall;
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		//
		String classAndMethodName = className+".channelActive";
		callLog.debug("classAndMethodName={}", classAndMethodName);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		//
		String classAndMethodName = className+".channelInactive";
		callLog.debug("classAndMethodName={}", classAndMethodName);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		//
		String classAndMethodName = className+".exceptionCaught";
		callLog.debug("classAndMethodName={}", classAndMethodName, cause);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext arg0, HttpObject msg) throws Exception {
		//
		String classAndMethodName = className+".channelRead0";
		
		FullHttpResponse response = (FullHttpResponse) msg;
    	String content = new String(response.content().copy().array());
    	
		callLog.debug("classAndMethodName={}, content={}", classAndMethodName, content);
    	
    	byte[] contentByte = new byte[response.content().readableBytes()];
    	response.content().readBytes(contentByte);
    	
    	content = content.replace("/bootstrap/3.3.2/css/bootstrap.min.css", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css");
    	content = content.replace("/bootstrap/3.3.2/css/bootstrap-theme.min.css", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css");
    	content = content.replace("/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css", "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js");
    	content = content.replace("/jquery/3.3.1/jquery.min.js", "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
    	content = content.replace("/bootstrap/3.3.2/js/bootstrap.min.js", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
    	content = content.replace("/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js", "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js");
    	content = content.replace("/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.ko.js", "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ko.min.js");

    	 if(recvCall != null) {
    		 //
      		callLog.debug("classAndMethodName={}, recvCall is not null Send Recv", classAndMethodName);

             recvCall.sendResponse(HTTPResponseStatus.OK, content);
         }
    	 else {
       		callLog.debug("classAndMethodName={}, recvCall is null Send Fail", classAndMethodName);

    	 }
    }
}
